FROM node:lts-alpine

# создание директории приложения
WORKDIR /usr/src/app

COPY . .

RUN yarn install


RUN yarn build 


EXPOSE 4000
CMD [ "node", "dist" ]