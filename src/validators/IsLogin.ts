import { AuthUserData } from './../type/AuthUserData';
import { ROLES_CONST } from '../consts/RolesConst';
import { MyContext } from './../type/MyContext';
import { createMethodDecorator, ResolverData } from 'type-graphql';
import JWTService from '../lib/JWTService';
import { UserEntity } from '../entities/UserEntity';
import { AuthenticationError } from 'apollo-server-express';

export function Authorized(role?: ROLES_CONST): MethodDecorator {
    return createMethodDecorator(async (resolve: ResolverData<MyContext>, next) => {
        const { context } = resolve;
        const { authorization } = context.req.headers;
        if (authorization) {
            const [bearer, token] = authorization.split(' ');
            if (bearer == 'Bearer')
                try {
                    const tokeData = await JWTService.validateToken(token);
                    if (tokeData) {
                        const { data } = tokeData as { data: AuthUserData };
                        if (role) {
                            const userWasFound = UserEntity.findOne({ id: data.id, role });
                            if (!userWasFound) throw new AuthenticationError("User don't have access rights");
                            return next();
                        } else {
                            context.userData = data;
                            return next();
                        }
                    }
                } catch (error) {
                    throw new AuthenticationError(error);
                }
        }
        throw new AuthenticationError('User is not authorized');
    });
}
