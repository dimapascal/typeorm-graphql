import { ChatEntity } from './../entities/ChatEntity';
import {
    registerDecorator,
    ValidationOptions,
    ValidatorConstraint,
    ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ async: true })
export class IsChatIdConstraint implements ValidatorConstraintInterface {
    async validate(id: string): Promise<boolean> {
        return await ChatEntity.findOne({ id }).then((user) => !!user);
    }

    defaultMessage(): string {
        return 'chat with that id does not exist';
    }
}

export function IsChatId(validationOptions?: ValidationOptions) {
    // eslint-disable-next-line
    return function (object: Object, propertyName: string): void {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsChatIdConstraint,
        });
    };
}
