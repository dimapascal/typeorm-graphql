import { UserEntity } from '../entities/UserEntity';
import {
    registerDecorator,
    ValidationOptions,
    ValidatorConstraint,
    ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ async: true })
export class IsEmailAlreadyExistConstraint implements ValidatorConstraintInterface {
    async validate(email: string): Promise<boolean> {
        if (!email) {
            return false;
        }
        return await UserEntity.findOne({ where: { email } }).then((user) => !user);
    }

    defaultMessage(): string {
        return 'email is already used!';
    }
}

export function IsEmailAlreadyExist(validationOptions?: ValidationOptions) {
    // eslint-disable-next-line
    return function (object: Object, propertyName: string): void {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsEmailAlreadyExistConstraint,
        });
    };
}
