export type JWTResponse = {
    token: string;
    refreshToken: string;
    expiresIn: number;
};
