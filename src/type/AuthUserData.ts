export type AuthUserData = {
    id: string;
    email: string;
};
