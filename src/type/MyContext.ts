import { AuthUserData } from './AuthUserData';
import { Request } from 'express';
import { Connection } from 'typeorm';
import { PubSub } from 'graphql-subscriptions';

export type MyContext = {
    req: Request;
    connection: Connection;
    userData: AuthUserData;
    pubSub: PubSub;
};
