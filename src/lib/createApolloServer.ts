import { buildSchema } from 'type-graphql';
import { ApolloServer } from 'apollo-server-express';
import { MyContext } from '../type/MyContext';
import { getApolloConfig } from '../configs/ApolloConfig';
import { join } from 'path';

export async function createApolloServer(context: Partial<MyContext>): Promise<ApolloServer> {
    const { resolvers_path } = getApolloConfig();

    const schema = await buildSchema({
        resolvers: [join(__dirname, '..', resolvers_path)],
    });

    const apolloServer = new ApolloServer({
        schema,
        context: ({ req }) => ({ req, ...context } as MyContext),
    });

    return apolloServer;
}
