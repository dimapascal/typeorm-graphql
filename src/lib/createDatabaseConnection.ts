import { getDatabaseConfig } from '../configs/DatabaseConfig';
import { Connection, createConnection } from 'typeorm';
import { join } from 'path';
import { InitialSeedDatabase } from '../migrations/InitialSeedDatabase';

export async function createDatabaseConnection(): Promise<Connection> {
    const { connection_type, entities_path, seed_database, ...options } = getDatabaseConfig();

    const connection = await createConnection({
        ...options,
        // eslint-disable-next-line
        type: connection_type as any,
        entities: [join(__dirname, '..', entities_path)],
    });

    if (seed_database) InitialSeedDatabase(connection);

    return connection;
}
