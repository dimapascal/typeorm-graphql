import { AuthUserData } from './../type/AuthUserData';
import { JWTResponse } from './../type/JWTResponse';
import { getJwtConfig } from './../configs/JwtConfig';
import jwt, { VerifyErrors } from 'jsonwebtoken';

class JWTService {
    private secret!: string;
    private expiresIn!: number;
    private refreshExpiresIn!: number;

    constructor() {
        const { secret, expiresIn, refreshExpiresIn } = getJwtConfig();
        this.secret = secret;
        this.expiresIn = expiresIn;
        this.refreshExpiresIn = refreshExpiresIn;
    }

    createToken(data: AuthUserData): JWTResponse {
        const token = jwt.sign({ data }, this.secret, { expiresIn: this.expiresIn });
        const refreshToken = jwt.sign({ data }, this.secret, { expiresIn: this.refreshExpiresIn });
        return {
            token,
            refreshToken,
            expiresIn: this.expiresIn,
        };
    }

    validateToken(token: string): Promise<VerifyErrors | Object | undefined> {
        return new Promise((resolve, reject) => {
            jwt.verify(token, this.secret, (err: VerifyErrors | null, data: Object | undefined) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    async updateJWTUsingRefreshToken(refreshToken: string): Promise<VerifyErrors | Object | undefined> {
        const response = await this.validateToken(refreshToken);
        if (response) {
            const { data }: any = response;
            return this.createToken(data as AuthUserData);
        }
        return undefined;
    }
}

export default new JWTService();
