import { getEncryptionConfig } from './../configs/EncryptionConfig';

const { base64_additional_string } = getEncryptionConfig();

export const encryptStringInBase64 = (string: string, additionalString = ''): string =>
    Buffer.from(string + base64_additional_string + additionalString, 'ascii').toString('base64');

export const decryptStringInBase64 = (base64: string, additionalString = ''): string =>
    Buffer.from(base64, 'base64').toString('ascii').replace(base64_additional_string, '').replace(additionalString, '');
