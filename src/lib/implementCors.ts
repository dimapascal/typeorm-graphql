import { Express } from 'express';
import cors from 'cors';
import { getCorsConfig } from '../configs/CorsConfig';

export function implementCors(app: Express): Express {
    const { cors_origin } = getCorsConfig();
    app.use(
        cors({
            credentials: true,
            origin: cors_origin,
        })
    );
    return app;
}
