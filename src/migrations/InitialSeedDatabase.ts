import { Connection } from 'typeorm';
import { SeedRolesTable } from './SeedRolesTable';

export async function InitialSeedDatabase(connection: Connection): Promise<void> {
    await SeedRolesTable(connection);
}
