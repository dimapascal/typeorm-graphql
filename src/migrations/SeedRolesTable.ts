import { RolesObjectType, ROLES_OBJECT_CONST } from './../consts/RolesConst';
import { RoleEntity } from '../entities/RoleEntity';
import { Connection } from 'typeorm';

export async function SeedRolesTable(connection: Connection): Promise<(RolesObjectType & RoleEntity)[]> {
    const responses = [];
    for (const role of Object.values(ROLES_OBJECT_CONST)) {
        const response = await connection.getRepository(RoleEntity).save(role);
        responses.push(response);
    }
    return responses;
}
