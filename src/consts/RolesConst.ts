export type RolesObjectType = {
    id: number;
    name: string;
};

export enum ROLES_CONST {
    USER = 1,
    ADMIN = 2,
    SUPPER_ADMIN = 3,
}

export const ROLES_OBJECT_CONST: { [key: string]: RolesObjectType } = {
    USER: { id: ROLES_CONST.USER, name: 'user' },
    ADMIN: { id: ROLES_CONST.ADMIN, name: 'admin' },
    SUPPER_ADMIN: { id: ROLES_CONST.SUPPER_ADMIN, name: 'supper_admin' },
};
