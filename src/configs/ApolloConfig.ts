import { Config, EnvValue } from 'type-env';

@Config
class ApolloConfig {
    @EnvValue('APOLLO_RESOLVERS_PATH') resolvers_path: string;
}

const config = new ApolloConfig();

export const getApolloConfig = (): ApolloConfig => config;
