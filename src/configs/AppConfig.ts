import { Config, EnvValue } from 'type-env';

type IsProdType = {
    isProd: boolean;
};

@Config
class AppConfig {
    @EnvValue('NODE_ENV') node_env: string;
    @EnvValue('APP_PORT') app_port: number;
}

const config = new AppConfig();

export const getAppConfig = (): AppConfig & IsProdType => ({ ...config, isProd: config.node_env === 'production' });
