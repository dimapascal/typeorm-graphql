import { Config, EnvValue } from 'type-env';

@Config
class EncryptionConfig {
    @EnvValue('SALT_ROUNDS') salt_rounds: number;
    @EnvValue('BASE64_ADDITIONAL_STRING') base64_additional_string: string;
}

const config = new EncryptionConfig();

export const getEncryptionConfig = (): EncryptionConfig => config;
