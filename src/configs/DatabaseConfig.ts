import { DatabaseType } from 'typeorm';
import { Config, EnvValue } from 'type-env';

@Config
class DatabaseConfig {
    @EnvValue('DB_HOST') host: string;
    @EnvValue('DB_PORT') port: number;
    @EnvValue('DB_USERNAME') username: string;
    @EnvValue('DB_PASSWORD') password: string;
    @EnvValue('DB_NAME') database: string;
    @EnvValue('DB_CONNECTION_TYPE') connection_type: DatabaseType = 'postgres';
    @EnvValue('DB_SYNCHRONIZE') synchronize = true;
    @EnvValue('DB_LOGGING') logging = true;
    @EnvValue('DB_ENTITIES_PATH') entities_path: string;
    @EnvValue('DB_SEED') seed_database: boolean;
}

const config = new DatabaseConfig();

export const getDatabaseConfig = (): DatabaseConfig => config;
