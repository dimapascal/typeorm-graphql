import { Config, EnvValue } from 'type-env';

@Config
class JwtConfig {
    @EnvValue('JWT_SECRET') secret: string;
    @EnvValue('JWT_EXPIRES_IN') expiresIn: number;
    @EnvValue('JWT_REFRESH_EXPIRES_IN') refreshExpiresIn: number;
}

const config = new JwtConfig();

export const getJwtConfig = (): JwtConfig => config;
