import { Config, EnvValue } from 'type-env';

@Config
class CorsConfig {
    @EnvValue('CORS_ORIGIN') cors_origin: string;
}

const config = new CorsConfig();

export const getCorsConfig = (): CorsConfig => config;
