import { PostMessageInput } from './Inputs/PostMessageInput';
import { ChatEntity } from './../../entities/ChatEntity';
import { MessagePayload } from './Payloads/MessagePayload';
import { MessageEntity } from './../../entities/MessageEntity';
import { MyContext } from './../../type/MyContext';
import { Arg, Ctx, Mutation, Resolver, Subscription, Root } from 'type-graphql';
import { Authorized } from '../../validators/IsLogin';
import { encryptStringInBase64, decryptStringInBase64 } from '../../lib/parseStringInBase64';

type ChatMessageSubscriptionArgs = { subscriptionId: string };

@Resolver()
export class MessagesResolver {
    @Authorized()
    @Mutation(() => Boolean)
    async postMessage(
        @Arg('options') { chatId, content, subscriptionId }: PostMessageInput,
        @Ctx() { userData, pubSub }: MyContext
    ): Promise<boolean> {
        try {
            const createdMessage = MessageEntity.create({ authorId: userData.id, chatId, content });
            const message = await createdMessage.save();
            const decryptedConnectionId = decryptStringInBase64(subscriptionId, userData.id);
            await pubSub.publish(decryptedConnectionId, { message } as MessagePayload);
            return true;
        } catch (error) {
            throw error;
        }
    }

    @Authorized()
    @Mutation(() => String, { nullable: true })
    async createChatMessageSubscription(
        @Arg('chatId') chatId: string,
        @Ctx() { pubSub, userData }: MyContext
    ): Promise<string | null> {
        const onMessage = (payload: MessagePayload) => payload;
        const chatExist = await ChatEntity.findOne({ id: chatId });
        if (chatExist) {
            const connectionId = await pubSub.subscribe(chatId.toString(), onMessage);
            return encryptStringInBase64(connectionId.toString(), userData.id);
        }
        return null;
    }

    @Authorized()
    @Mutation(() => Boolean)
    async deleteChatMessageSubscription(
        @Arg('subscriptionId') subscriptionId: string,
        @Ctx() { pubSub, userData }: MyContext
    ): Promise<boolean> {
        try {
            const decryptedConnectionId = decryptStringInBase64(subscriptionId, userData.id);
            await pubSub.unsubscribe(parseInt(decryptedConnectionId));
            return true;
        } catch (error) {
            return false;
        }
    }

    @Subscription({
        subscribe: (_, { subscriptionId }: ChatMessageSubscriptionArgs, { pubSub, userData }: MyContext) => {
            const decryptedConnectionId = decryptStringInBase64(subscriptionId, userData.id);
            return pubSub.asyncIterator(decryptedConnectionId);
        },
    })
    chatMessageSubscription(@Arg('subscriptionId') _: string, @Root() { message }: MessagePayload): MessageEntity {
        return message;
    }
}
