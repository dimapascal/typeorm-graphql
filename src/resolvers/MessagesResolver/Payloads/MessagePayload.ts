import { MessageEntity } from './../../../entities/MessageEntity';

export type MessagePayload = { message: MessageEntity };
