import { InputType, Field } from 'type-graphql';
import { IsString } from 'class-validator';
import { IsChatId } from '../../../validators/IsChatId';

@InputType()
export class PostMessageInput {
    @Field(() => String)
    @IsString()
    content: string;

    @Field(() => String)
    @IsString()
    @IsChatId()
    chatId: string;

    @Field(() => String)
    @IsString()
    subscriptionId: string;
}
