import { UserEntity } from './../../entities/UserEntity';
import { ChatEntity } from './../../entities/ChatEntity';
import { MyContext } from '../../type/MyContext';
import { Arg, Ctx, Resolver, Mutation } from 'type-graphql';
import { Authorized } from '../../validators/IsLogin';

@Resolver()
export class ChatResolver {
    @Authorized()
    @Mutation(() => ChatEntity)
    async createChat(@Arg('name') name: string, @Ctx() { userData }: MyContext): Promise<ChatEntity> {
        const author = (await UserEntity.findOne(userData.id)) as UserEntity;
        const chat = ChatEntity.create({
            name,
            authorId: userData.id,
        });

        chat.subscribers = [author];

        return await chat.save();
    }
}
