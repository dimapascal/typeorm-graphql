import { MyContext } from './../../type/MyContext';
import { JWTResponseObject } from './Responses/JWTResponseObject';
import { JWTResponse } from '../../type/JWTResponse';
import { UserEntity } from '../../entities/UserEntity';
import { getEncryptionConfig } from '../../configs/EncryptionConfig';
import { LoginInput } from './Inputs/LoginInput';
import { Arg, Ctx, Mutation, Resolver } from 'type-graphql';
import { RegisterInput } from './Inputs/RegisterInput';
import bcrypt from 'bcrypt';
import JWTService from '../../lib/JWTService';

@Resolver()
export class AuthResolver {
    @Mutation(() => Boolean)
    async register(
        @Arg('options') { password, ...options }: RegisterInput,
        @Ctx() { connection }: MyContext
    ): Promise<boolean> {
        const { salt_rounds } = getEncryptionConfig();
        const hashedPassword = bcrypt.hashSync(password, salt_rounds);
        const userToCreate = { ...options, password: hashedPassword };

        const user = connection.createQueryBuilder().insert().into(UserEntity).values(userToCreate).execute();

        return !!user;
    }

    @Mutation(() => JWTResponseObject, { nullable: true })
    async login(@Arg('options') options: LoginInput): Promise<JWTResponse | null> {
        const user = await UserEntity.findOne({ where: { email: options.email } });

        if (!user) return null;

        if (!bcrypt.compareSync(options.password, user.password)) return null;

        const { id, email } = user;
        return JWTService.createToken({ id, email });
    }

    @Mutation(() => JWTResponseObject, { nullable: true })
    async refreshToken(@Arg('token') token: string): Promise<JWTResponse | null> {
        try {
            const data = await JWTService.updateJWTUsingRefreshToken(token);
            return (data as JWTResponse) || null;
        } catch (error) {
            return null;
        }
    }
}
