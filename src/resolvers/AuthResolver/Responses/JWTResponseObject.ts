import { Field, ObjectType } from 'type-graphql';

@ObjectType()
export class JWTResponseObject {
    @Field(() => String)
    token!: string;

    @Field(() => String)
    refreshToken!: string;

    @Field(() => Number)
    expiresIn!: number;
}
