import { IsEmailAlreadyExist } from '../../../validators/IsEmailAlreadyExist';
import { InputType, Field } from 'type-graphql';
import { IsEmail, Length } from 'class-validator';

@InputType()
export class RegisterInput {
    @Field()
    @Length(1, 255)
    firstName: string;

    @Field()
    @Length(1, 255)
    lastName: string;

    @Field()
    @Length(1, 255)
    @IsEmail()
    @IsEmailAlreadyExist()
    email: string;

    @Field()
    age: number;

    @Field()
    @Length(4, 255)
    password: string;
}
