import { UserEntity } from '../../entities/UserEntity';
import { MyContext } from '../../type/MyContext';
import { Ctx, Query, Resolver } from 'type-graphql';
import { Authorized } from '../../validators/IsLogin';

@Resolver()
export class UserResolver {
    @Authorized()
    @Query(() => String)
    hello(): string {
        return 'Hello world';
    }

    @Query(() => [UserEntity])
    async users(@Ctx() { connection }: MyContext): Promise<Array<UserEntity>> {
        const users = await connection.getRepository(UserEntity).createQueryBuilder('user').getMany();
        return users;
    }

    @Authorized()
    @Query(() => UserEntity, { nullable: true })
    async me(@Ctx() { userData }: MyContext): Promise<UserEntity | null> {
        if (userData?.id) {
            const user = await UserEntity.findOne(userData.id);
            return user || null;
        }
        return null;
    }
}
