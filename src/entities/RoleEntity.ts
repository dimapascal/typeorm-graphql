import { UserEntity } from './UserEntity';
import { Field, ID, ObjectType } from 'type-graphql';
import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany } from 'typeorm';

@ObjectType()
@Entity('role')
export class RoleEntity extends BaseEntity {
    @Field(() => ID)
    @PrimaryGeneratedColumn()
    id!: number;

    @Field(() => String)
    @Column({ type: 'varchar' })
    name: string;

    @Field(() => [UserEntity])
    @OneToMany(() => UserEntity, (user) => user.roleId)
    users: [UserEntity];
}
