import { MANY2MANY_ENTITIES_NAMES } from './../consts/Many2ManyEntitiesNames';
import { MessageEntity } from './MessageEntity';
import { UserEntity } from './UserEntity';
import { Field, ID, ObjectType } from 'type-graphql';
import {
    Entity,
    PrimaryGeneratedColumn,
    BaseEntity,
    ManyToOne,
    Column,
    ManyToMany,
    JoinTable,
    OneToMany,
} from 'typeorm';

@ObjectType()
@Entity('chat')
export class ChatEntity extends BaseEntity {
    @Field(() => ID)
    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @ManyToMany(() => UserEntity, (user) => user.chats)
    @JoinTable({
        name: MANY2MANY_ENTITIES_NAMES.USERS_CHATS,
        joinColumn: { name: 'chat_id', referencedColumnName: 'id' },
        inverseJoinColumn: { name: 'user_id', referencedColumnName: 'id' },
    })
    subscribers: [UserEntity];

    @OneToMany(() => MessageEntity, (message) => message.chatId, { nullable: true })
    messages: [MessageEntity];

    @Field(() => String)
    @Column()
    authorId: string;

    // @Field(() => UserEntity)
    @ManyToOne(() => UserEntity, (user) => user.id)
    author: UserEntity;

    @Field(() => String)
    @Column({ type: 'varchar' })
    name: string;
}
