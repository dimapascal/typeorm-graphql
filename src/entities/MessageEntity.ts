import { ChatEntity } from './ChatEntity';
import { UserEntity } from './UserEntity';
import { Field, ID, ObjectType } from 'type-graphql';
import { Entity, PrimaryGeneratedColumn, BaseEntity, ManyToOne, Column } from 'typeorm';

@ObjectType()
@Entity('message')
export class MessageEntity extends BaseEntity {
    @Field(() => ID)
    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @ManyToOne(() => ChatEntity, (chat) => chat.id)
    chat: ChatEntity;

    @Field(() => String)
    @Column()
    chatId: string;

    // @Field(() => UserEntity)
    @ManyToOne(() => UserEntity, (user) => user.id)
    author: UserEntity;

    @Field(() => String)
    @Column()
    authorId: string;

    @Field(() => String)
    @Column({ type: 'varchar' })
    content: string;
}
