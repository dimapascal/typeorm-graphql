import { ChatEntity } from './ChatEntity';
import { MessageEntity } from './MessageEntity';
import { ROLES_CONST } from './../consts/RolesConst';
import { RoleEntity } from './RoleEntity';
import { Field, ID, ObjectType, Root } from 'type-graphql';
import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, OneToMany, ManyToMany } from 'typeorm';

@ObjectType()
@Entity('user')
export class UserEntity extends BaseEntity {
    @Field(() => ID)
    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @Field(() => String)
    @Column({ type: 'varchar' })
    firstName: string;

    @Field(() => String)
    @Column({ type: 'varchar' })
    lastName: string;

    @Field(() => String)
    @Column({ type: 'varchar', unique: true })
    email: string;

    @Field()
    name(@Root() { firstName, lastName }: UserEntity): string {
        return `${firstName} ${lastName}`;
    }

    @Field(() => Number)
    @Column({ type: 'numeric' })
    age: number;

    @ManyToMany(() => ChatEntity, (chat) => chat.subscribers, { nullable: true })
    chats: [ChatEntity];

    @Field(() => RoleEntity)
    @ManyToOne(() => RoleEntity, (role) => role.id)
    async role(): Promise<RoleEntity> {
        const { roleId } = this;
        const role = await RoleEntity.findOne({ id: roleId });
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        return role!;
    }

    @Field(() => Number)
    @Column({ type: 'numeric', default: ROLES_CONST.USER })
    roleId: number;

    @OneToMany(() => MessageEntity, (message) => message.authorId, { nullable: true })
    messages: [UserEntity];

    @Column({ type: 'varchar' })
    password: string;
}
