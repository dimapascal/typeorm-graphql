import 'reflect-metadata';
import Dotenv from 'dotenv';
import Express from 'express';
import { getAppConfig } from './configs/AppConfig';
import { createDatabaseConnection } from './lib/createDatabaseConnection';
import { implementCors } from './lib/implementCors';
import { createApolloServer } from './lib/createApolloServer';
import http from 'http';
import { PubSub } from 'graphql-subscriptions';

Dotenv.config();

const bootstrap = async () => {
    const { app_port } = getAppConfig();
    const pubSub = new PubSub();

    // Create server
    const app = implementCors(Express());

    const httpServer = http.createServer(app);
    // Create server

    // Create Database connection
    const connection = await createDatabaseConnection();
    // Create Database connection

    // Create and connect apollo server
    const apolloServer = await createApolloServer({ connection, pubSub });
    apolloServer.applyMiddleware({ app });
    // Create and connect apollo server

    // Implement apollo subscriptions in server
    apolloServer.installSubscriptionHandlers(httpServer);
    // Implement apollo subscriptions in server

    // Run server on app_port
    httpServer.listen(app_port, () => {
        console.log('\x1b[32m', 'Server started on port:', app_port);
    });
    // Run server on app_port
};

bootstrap();
